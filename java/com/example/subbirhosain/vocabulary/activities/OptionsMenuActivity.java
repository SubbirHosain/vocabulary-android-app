package com.example.subbirhosain.vocabulary.activities;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.subbirhosain.vocabulary.R;

public class OptionsMenuActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.action_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.about_us:
                Intent intent = new Intent(getApplicationContext(),AboutMe.class);
                startActivity(intent);
                return true;
            case R.id.tab:
                Intent tabIntent = new Intent(getApplicationContext(),FrongPageActivity.class);
                startActivity(tabIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
