package com.example.subbirhosain.vocabulary.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.subbirhosain.vocabulary.R;
import com.example.subbirhosain.vocabulary.adapters.VocabulariesAdapter;
import com.example.subbirhosain.vocabulary.adapters.WordsLocalDatabaseAdapter;
import com.example.subbirhosain.vocabulary.database.AppDatabase;
import com.example.subbirhosain.vocabulary.database.WordCategory;
import com.example.subbirhosain.vocabulary.database.Words;
import com.example.subbirhosain.vocabulary.models.Vocabularies;
import com.example.subbirhosain.vocabulary.models.Vocabulary;
import com.example.subbirhosain.vocabulary.models.WordCategories;
import com.example.subbirhosain.vocabulary.network.ApiClient;
import com.example.subbirhosain.vocabulary.network.ApiService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends OptionsMenuActivity {
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;

    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    VocabulariesAdapter vocabulariesAdapter;
    WordsLocalDatabaseAdapter wordsLocalDatabaseAdapter;
    //vocabularies model class
    List<Vocabulary> vocabularyList;

    private Spinner wordCategorySpinner;
    List<String> wordCategoryNameList;
    List<Integer> wordCategoryIdList;
    int wordCategoryId;
    private ProgressBar progressBar;
    AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = AppDatabase.getAppDatabase(MainActivity.this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Find the UI components
        recyclerView = (RecyclerView) findViewById(R.id.wordList);

        // Arrange the items linearly
        layoutManager = new LinearLayoutManager(this);

        // Get the Words list

        // Create the WordAdapter
        vocabulariesAdapter = new VocabulariesAdapter(MainActivity.this, vocabularyList);

        // Set the LayoutManager
        recyclerView.setLayoutManager(layoutManager);

        // Set the Adapter
        recyclerView.setAdapter(vocabulariesAdapter);

        //word category spinner
        wordCategorySpinner = findViewById(R.id.word_category_spinner);
        progressBar = findViewById(R.id.progressBar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.navigation_menu);
        navigationView.inflateMenu(R.menu.drawer_menu);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        Toast.makeText(getApplicationContext(), "home", Toast.LENGTH_SHORT).show();
                        break;
                }
                return false;
            }
        });


        //get word lists
        if (isNetworkAvailable()) {
            //clear all tables and insert again
            //db.clearAllTables();
            //load from remote db and save to local db
            //get word categories in spinner
            getWordCategories();
            getWordLists();

        } else {

            //load word category from local db
            List<WordCategory> localSpinner = db.getWordsDao().getWordCategories();

            List<String> local = new ArrayList<String>();
            wordCategoryIdList = new ArrayList<Integer>();

            local.add(0, "Choose Word Category");
            wordCategoryIdList.add(0, 0);

            for (int k = 0; k < localSpinner.size(); k++) {
                local.add(localSpinner.get(k).getWordCategoryName());
                wordCategoryIdList.add(localSpinner.get(k).getWordCategoryId());
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                    android.R.layout.simple_spinner_item, local);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            wordCategorySpinner.setAdapter(adapter);


            //load from local database using room
            //Toast.makeText(MainActivity.this, "no connection", Toast.LENGTH_SHORT).show();
            List<Words> x = db.getWordsDao().getAllWords();
            wordsLocalDatabaseAdapter = new WordsLocalDatabaseAdapter(MainActivity.this, x);
            recyclerView.setAdapter(wordsLocalDatabaseAdapter);
        /*
           for (int j=0;j<x.size();j++){
               Toast.makeText(MainActivity.this,x.get(j).getEnglishWord(),Toast.LENGTH_SHORT).show();
           }
        */

        }
        //on change and select
        wordCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getItemAtPosition(i).equals("Choose Word Category")) {
                    //do nothing
                } else {
                    String selectedName = adapterView.getItemAtPosition(i).toString();
                    wordCategoryId = wordCategoryIdList.get(i);

                    //Toast.makeText(getApplicationContext(), "value:" + wordCategoryId, Toast.LENGTH_SHORT).show();

                    //on item select fetch associated categorey  voabulary
                    if (isNetworkAvailable()) {
                        ApiService service = ApiClient.getClient().create(ApiService.class);
                        Call<Vocabularies> call = service.getCategorizedWords(wordCategoryId);
                        call.enqueue(new Callback<Vocabularies>() {
                            @Override
                            public void onResponse(Call<Vocabularies> call, Response<Vocabularies> response) {
                                List<Vocabulary> vocabularyList1 = response.body().getVocabularies();
                                vocabulariesAdapter = new VocabulariesAdapter(MainActivity.this, vocabularyList1);
                                recyclerView.setAdapter(vocabulariesAdapter);

                            }

                            @Override
                            public void onFailure(Call<Vocabularies> call, Throwable t) {

                            }
                        });
                    } else {
                        //fetch from local db
                        List<Words> p = db.getWordsDao().findVocabularyByCategory(wordCategoryId);
                        wordsLocalDatabaseAdapter = new WordsLocalDatabaseAdapter(MainActivity.this,p);
                        recyclerView.setAdapter(wordsLocalDatabaseAdapter);
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    //get word categories in spinner
    private void getWordCategories() {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<List<WordCategories>> call = service.getWordCategories();
        call.enqueue(new Callback<List<WordCategories>>() {
            @Override
            public void onResponse(Call<List<WordCategories>> call, Response<List<WordCategories>> response) {
                List<WordCategories> wordCategories = response.body();
                wordCategoryNameList = new ArrayList<String>();
                wordCategoryIdList = new ArrayList<Integer>();

                wordCategoryNameList.add(0, "Choose Word Category");
                wordCategoryIdList.add(0, 0);

                //adding the spinner text and id in list
                for (int i = 0; i < wordCategories.size(); i++) {
                    wordCategoryNameList.add(wordCategories.get(i).getWordCategoryName());
                    wordCategoryIdList.add(wordCategories.get(i).getWordCategoryId());

                    //for inserting in local db
                    WordCategory wordCategory = new WordCategory();
                    wordCategory.setWordCategoryId(wordCategories.get(i).getWordCategoryId());
                    wordCategory.setWordCategoryName(wordCategories.get(i).getWordCategoryName());
                    db.getWordsDao().insertCategory(wordCategory);
                }
                //set the adapter
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                        android.R.layout.simple_spinner_item, wordCategoryNameList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                wordCategorySpinner.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<WordCategories>> call, Throwable t) {

            }
        });
    }

    //get word lists
    private void getWordLists() {
        progressBar.setVisibility(View.VISIBLE);
        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<Vocabularies> call = service.getWords();
        call.enqueue(new Callback<Vocabularies>() {
            @Override
            public void onResponse(Call<Vocabularies> call, Response<Vocabularies> response) {

                vocabularyList = response.body().getVocabularies();
                vocabulariesAdapter.setWords(vocabularyList);
                progressBar.setVisibility(View.GONE);

                //before inserting new entries clear old entries
                //we ll track number of rows with sahredpref and then decide to clear tables and insert
                //db.clearAllTables();
                //inset the json into sqlite
                for (int i = 0; i < vocabularyList.size(); i++) {
                    Words x = new Words();
                    x.setWordId(vocabularyList.get(i).getWordId());
                    x.setEnglishWord(vocabularyList.get(i).getEnglishWord());
                    x.setBanglaMeaning(vocabularyList.get(i).getBanglaMeaning());
                    x.setEnglishExample(vocabularyList.get(i).getEnglishExample());
                    x.setWordCategoryId(vocabularyList.get(i).getWordCategoryId());
                    //AppDatabase.getAppDatabase(getApplicationContext()).getWordsDao().insertAll(x);
                    db.getWordsDao().insertAll(x);
                }
                Toast.makeText(MainActivity.this, "done inserting", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Vocabularies> call, Throwable t) {
                //wordList = null;
                progressBar.setVisibility(View.GONE);
            }
        });

    }

    //internet connection available or not
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
