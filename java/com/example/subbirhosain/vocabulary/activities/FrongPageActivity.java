package com.example.subbirhosain.vocabulary.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.example.subbirhosain.vocabulary.R;
import com.example.subbirhosain.vocabulary.adapters.ViewPagerAdapter;

public class FrongPageActivity extends OptionsMenuActivity implements TabLayout.OnTabSelectedListener {
    ViewPager simpleViewPager;
    TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frong_page);

        // get the reference of ViewPager and TabLayout
        simpleViewPager = (ViewPager) findViewById(R.id.simpleViewPager);
        tabLayout = (TabLayout) findViewById(R.id.simpleTabLayout);

        // Create a new Tab named "First"
        TabLayout.Tab homeTab = tabLayout.newTab();
        homeTab.setText("WordLists"); // set the Text for the first Tab
        homeTab.setIcon(R.mipmap.ic_launcher); // set an icon for the
        // first tab
        tabLayout.addTab(homeTab); // add  the tab at in the TabLayout

        // Create a new Tab named "RevisionLists"
        TabLayout.Tab firstTab = tabLayout.newTab();
        firstTab.setText("RevisionLists"); // set the Text for the first Tab
        firstTab.setIcon(R.mipmap.ic_launcher); // set an icon for the
        // first tab
        tabLayout.addTab(firstTab); // add  the tab at in the TabLayout

        // Create a new Tab named "Random"
        TabLayout.Tab randomTab = tabLayout.newTab();
        randomTab.setText("RandomWords"); // set the Text for the first Tab
        randomTab.setIcon(R.mipmap.ic_launcher); // set an icon for the
        // first tab
        tabLayout.addTab(randomTab); // add  the tab at in the TabLayout

        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        simpleViewPager.setAdapter(pagerAdapter);

        // addOnPageChangeListener event change the tab on slide
        simpleViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(this);

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        simpleViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
