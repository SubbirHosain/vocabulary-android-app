package com.example.subbirhosain.vocabulary.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "word_category")
public class WordCategory {

    @ColumnInfo(name = "word_category_id")
    @PrimaryKey
    @NonNull
    private int wordCategoryId;

    @ColumnInfo(name = "word_category_name")
    private String wordCategoryName;


    public int getWordCategoryId() {
        return wordCategoryId;
    }

    public void setWordCategoryId(int wordCategoryId) {
        this.wordCategoryId = wordCategoryId;
    }

    public String getWordCategoryName() {
        return wordCategoryName;
    }

    public void setWordCategoryName(String wordCategoryName) {

        this.wordCategoryName = wordCategoryName;
    }
}
