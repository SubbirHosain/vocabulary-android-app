package com.example.subbirhosain.vocabulary.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface WordsDao {

    //ignore the old values and insert new values
    //@Insert(onConflict = OnConflictStrategy.IGNORE)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Words words);

    @Query("SELECT * FROM word ORDER BY word_id DESC")
    List<Words> getAllWords();

    //category inserting and reading
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCategory(WordCategory wordCategory);

    //SPINNER load from local db
    @Query("SELECT * FROM word_category")
    List<WordCategory> getWordCategories();

    //fetch vocabulary by id
    @Query("SELECT * FROM word WHERE word_category_id = :wordCategoryId ORDER BY word_id DESC")
    List<Words> findVocabularyByCategory(int wordCategoryId);
}
