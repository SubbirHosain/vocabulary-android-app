package com.example.subbirhosain.vocabulary.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "word")
public class Words {

    @ColumnInfo(name = "word_id")
    @PrimaryKey
    @NonNull
    private int wordId;

    @ColumnInfo(name = "english_word")
    private String englishWord;

    @ColumnInfo(name = "bangla_meaning")
    private String banglaMeaning;

    @ColumnInfo(name = "english_example")
    private String englishExample;

    @ColumnInfo(name = "bangla_example")
    private String banglaExample;

    @ColumnInfo(name = "word_category_id")
    private int wordCategoryId;

    public int getWordCategoryId() {
        return wordCategoryId;
    }

    public void setWordCategoryId(int wordCategoryId) {
        this.wordCategoryId = wordCategoryId;
    }


    public int getWordId() {
        return wordId;
    }

    public void setWordId(int wordId) {
        this.wordId = wordId;
    }

    public String getEnglishWord() {
        return englishWord;
    }

    public void setEnglishWord(String englishWord) {
        this.englishWord = englishWord;
    }

    public String getBanglaMeaning() {
        return banglaMeaning;
    }

    public void setBanglaMeaning(String banglaMeaning) {
        this.banglaMeaning = banglaMeaning;
    }

    public String getEnglishExample() {
        return englishExample;
    }

    public void setEnglishExample(String egnlishExample) {
        this.englishExample = egnlishExample;
    }

    public String getBanglaExample() {
        return banglaExample;
    }

    public void setBanglaExample(String banglaExample) {
        this.banglaExample = banglaExample;
    }
}
