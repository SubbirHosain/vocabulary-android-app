package com.example.subbirhosain.vocabulary.network;

import com.example.subbirhosain.vocabulary.models.Vocabularies;
import com.example.subbirhosain.vocabulary.models.WordCategories;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by SubbirHosain on 1/22/2018.
 */

public interface ApiService {

    @GET("get_word_list.php")
    Call<Vocabularies> getWords();

    //get word category list
    @GET("get_category_list.php")
    Call<List<WordCategories>> getWordCategories();

    //get categorized words list
    @GET("get_word_by_category.php")
    Call<Vocabularies> getCategorizedWords(@Query("word_category_id") int WordCategoryId);

    //get words by filter
    @GET("word_revise_by_filtering.php")
    Call<Vocabularies> getWordsByFiltering(@Query("filter") String WordFilter);

    //get random words
    @GET("get_random_words.php")
    Call<Vocabularies> getRandomWords();

    //get random words by category
    @GET("get_random_words_by_category.php")
    Call<Vocabularies> getRandomWordsByCategory(@Query("word_category_id") int WordCategory);

}

