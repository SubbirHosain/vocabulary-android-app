package com.example.subbirhosain.vocabulary.network;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by SubbirHosain on 1/22/2018.
 */

public class ApiClient {

    private static final String BASE_URL = "http://subbirhosain.com/vocabulary_api/";
    public static Retrofit retrofit = null;

    public static Retrofit getClient(){
        if(retrofit==null){

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new LoggingInterceptor()).build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        }
        return retrofit;
    }
}
