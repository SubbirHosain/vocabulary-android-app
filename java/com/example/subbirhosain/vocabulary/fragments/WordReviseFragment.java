package com.example.subbirhosain.vocabulary.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.subbirhosain.vocabulary.R;
import com.example.subbirhosain.vocabulary.adapters.VocabulariesAdapter;
import com.example.subbirhosain.vocabulary.models.Vocabularies;
import com.example.subbirhosain.vocabulary.models.Vocabulary;
import com.example.subbirhosain.vocabulary.network.ApiClient;
import com.example.subbirhosain.vocabulary.network.ApiService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class WordReviseFragment extends Fragment {

    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    VocabulariesAdapter vocabulariesAdapter;
    private Spinner wordCategoryFilter;
    String[] wordFiterOptions;
    View view;

    public WordReviseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_word_revise, container, false);
        wordCategoryFilter = (Spinner) view.findViewById(R.id.word_category_filter);

        //recyclerview list
        recyclerView = (RecyclerView) view.findViewById(R.id.wordReviseFragment);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        return view;
    }

    //get words by filter
    private void getWordsByFilter() {
        // Initializing a String Array
        wordFiterOptions = new String[]{
                "Choose Word Filter",
                "Daily",
                "Yesterday",
                "Weekly",
                "Monthly"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, wordFiterOptions);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        wordCategoryFilter.setAdapter(adapter);

    }

    //get words by filter
    private void getWordLists() {

        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<Vocabularies> call = service.getWords();
        call.enqueue(new Callback<Vocabularies>() {
            @Override
            public void onResponse(Call<Vocabularies> call, Response<Vocabularies> response) {
                List<Vocabulary> vocabularyList1 = response.body().getVocabularies();
                vocabulariesAdapter = new VocabulariesAdapter(getActivity(), vocabularyList1);
                recyclerView.setAdapter(vocabulariesAdapter);
            }

            @Override
            public void onFailure(Call<Vocabularies> call, Throwable t) {

            }
        });

    }

    @Override
    public void setUserVisibleHint(boolean isUserVisible) {
        super.setUserVisibleHint(isUserVisible);
        // when fragment visible to user and view is not null then enter here.
        if (isUserVisible && view != null) {
            onResume();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }
        //do your stuff here
        getWordsByFilter();
        getWordLists();
        wordCategoryFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getItemAtPosition(i).equals("Choose Word Filter")) {
                    //do nothing
                } else {

                    String selectedName = adapterView.getItemAtPosition(i).toString();
                    ApiService service = ApiClient.getClient().create(ApiService.class);
                    Call<Vocabularies> call = service.getWordsByFiltering(selectedName);

                    call.enqueue(new Callback<Vocabularies>() {
                        @Override
                        public void onResponse(Call<Vocabularies> call, Response<Vocabularies> response) {
                            List<Vocabulary> vocabularyList = response.body().getVocabularies();
                            vocabulariesAdapter = new VocabulariesAdapter(getActivity(), vocabularyList);
                            recyclerView.setAdapter(vocabulariesAdapter);
                        }

                        @Override
                        public void onFailure(Call<Vocabularies> call, Throwable t) {

                        }
                    });
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
