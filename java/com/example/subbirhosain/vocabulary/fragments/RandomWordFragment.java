package com.example.subbirhosain.vocabulary.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.subbirhosain.vocabulary.R;
import com.example.subbirhosain.vocabulary.adapters.VocabulariesAdapter;
import com.example.subbirhosain.vocabulary.models.Vocabularies;
import com.example.subbirhosain.vocabulary.models.Vocabulary;
import com.example.subbirhosain.vocabulary.models.WordCategories;
import com.example.subbirhosain.vocabulary.network.ApiClient;
import com.example.subbirhosain.vocabulary.network.ApiService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RandomWordFragment extends Fragment {
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager layoutManager;
    VocabulariesAdapter vocabulariesAdapter;

    private Spinner wordCategorySpinner;
    List<String> wordCategoryNameList;
    List<Integer> wordCategoryIdList;
    int wordCategoryId;
    View view;

    public RandomWordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_random_word, container, false);
        wordCategorySpinner = (Spinner) view.findViewById(R.id.word_category_spinner);
        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        swipeRefresh.setColorSchemeResources(R.color.colorAccent);
        recyclerView = (RecyclerView) view.findViewById(R.id.wordListFragment);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        return view;
    }

    //get word categories in spinner
    private void getWordCategories() {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<List<WordCategories>> call = service.getWordCategories();
        call.enqueue(new Callback<List<WordCategories>>() {
            @Override
            public void onResponse(Call<List<WordCategories>> call, Response<List<WordCategories>> response) {
                List<WordCategories> wordCategories = response.body();
                wordCategoryNameList = new ArrayList<String>();
                wordCategoryIdList = new ArrayList<Integer>();

                wordCategoryNameList.add(0, "Choose Word Category");
                wordCategoryIdList.add(0, 0);

                //adding the spinner text and id in list
                for (int i = 0; i < wordCategories.size(); i++) {
                    wordCategoryNameList.add(wordCategories.get(i).getWordCategoryName());
                    wordCategoryIdList.add(wordCategories.get(i).getWordCategoryId());
                }
                //set the adapter
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_spinner_item, wordCategoryNameList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                wordCategorySpinner.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<WordCategories>> call, Throwable t) {

            }
        });
    }

    //get word lists
    private void getWordLists() {

        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<Vocabularies> call = service.getRandomWords();
        call.enqueue(new Callback<Vocabularies>() {
            @Override
            public void onResponse(Call<Vocabularies> call, Response<Vocabularies> response) {
                List<Vocabulary> vocabularyList = response.body().getVocabularies();
                vocabulariesAdapter = new VocabulariesAdapter(getActivity(), vocabularyList);
                recyclerView.setAdapter(vocabulariesAdapter);
            }

            @Override
            public void onFailure(Call<Vocabularies> call, Throwable t) {

            }
        });

    }

    @Override
    public void setUserVisibleHint(boolean isUserVisible) {
        super.setUserVisibleHint(isUserVisible);
        // when fragment visible to user and view is not null then enter here.
        if (isUserVisible && view != null) {
            onResume();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }
        //do your stuff here
        getWordCategories();
        getWordLists();
        wordCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getItemAtPosition(i).equals("Choose Word Category")) {
                    //do nothing
                } else {
                    String selectedName = adapterView.getItemAtPosition(i).toString();
                    wordCategoryId = wordCategoryIdList.get(i);
                    //Toast.makeText(getApplicationContext(), "value:" + wordCategoryId, Toast.LENGTH_SHORT).show();
                    //on item select fetch associated voabulary
                    ApiService service = ApiClient.getClient().create(ApiService.class);
                    Call<Vocabularies> call = service.getRandomWordsByCategory(wordCategoryId);
                    call.enqueue(new Callback<Vocabularies>() {
                        @Override
                        public void onResponse(Call<Vocabularies> call, Response<Vocabularies> response) {
                            List<Vocabulary> vocabularyList1 = response.body().getVocabularies();
                            vocabulariesAdapter = new VocabulariesAdapter(getActivity(), vocabularyList1);
                            recyclerView.setAdapter(vocabulariesAdapter);

                        }

                        @Override
                        public void onFailure(Call<Vocabularies> call, Throwable t) {

                        }
                    });
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                        getWordLists();

                    }
                }, 2000);

            }
        });
    }
}
