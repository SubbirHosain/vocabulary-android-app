package com.example.subbirhosain.vocabulary.adapters;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.subbirhosain.vocabulary.R;
import com.example.subbirhosain.vocabulary.database.Words;

import java.util.List;
import java.util.Locale;

public class WordsLocalDatabaseAdapter extends RecyclerView.Adapter<WordsLocalDatabaseAdapter.WordsLocalDatabaseAdapterViewHolder> {

    private Context context;
    private List<Words> words;

    public WordsLocalDatabaseAdapter(Context context, List<Words> words) {
        this.context = context;
        this.words = words;
    }

    public void setWords(List<Words> words) {
        this.words = words;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public WordsLocalDatabaseAdapter.WordsLocalDatabaseAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.word_item, parent, false);
        return new WordsLocalDatabaseAdapter.WordsLocalDatabaseAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final WordsLocalDatabaseAdapter.WordsLocalDatabaseAdapterViewHolder holder, int position) {
        final Words word = words.get(position);
        holder.english_word.setText(word.getEnglishWord());
        holder.english_example.setText(word.getEnglishExample());

        //on double clik show a bangla meaning of the word
        holder.english_word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, word.getBanglaMeaning(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public int getItemCount() {
        if (words != null) {
            return words.size();
        }
        return 0;
    }

    public class WordsLocalDatabaseAdapterViewHolder extends RecyclerView.ViewHolder {
        TextToSpeech tts;
        TextView english_word, english_example;
        ImageButton speakBtn;

        public WordsLocalDatabaseAdapterViewHolder(View itemView) {
            super(itemView);

            english_word = (TextView) itemView.findViewById(R.id.english_word);
            english_example = (TextView) itemView.findViewById(R.id.english_example);
            speakBtn = itemView.findViewById(R.id.speakBtn);

            tts = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status != TextToSpeech.ERROR) {
                        tts.setLanguage(Locale.UK);
                    }
                }
            });
            speakBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String toSpeak = english_word.getText().toString();
                    tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH,null);
                }
            });
        }

    }
}
