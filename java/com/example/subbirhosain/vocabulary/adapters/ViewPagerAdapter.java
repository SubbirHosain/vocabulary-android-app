package com.example.subbirhosain.vocabulary.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.subbirhosain.vocabulary.fragments.RandomWordFragment;
import com.example.subbirhosain.vocabulary.fragments.WordReviseFragment;
import com.example.subbirhosain.vocabulary.fragments.WordsFragment;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public ViewPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;

    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new WordsFragment();
            case 1:
                return new WordReviseFragment();
            case 2:
                return new RandomWordFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}
