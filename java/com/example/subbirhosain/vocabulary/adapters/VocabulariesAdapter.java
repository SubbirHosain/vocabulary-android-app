package com.example.subbirhosain.vocabulary.adapters;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.subbirhosain.vocabulary.R;
import com.example.subbirhosain.vocabulary.models.Vocabulary;

import java.util.List;
import java.util.Locale;

public class VocabulariesAdapter extends RecyclerView.Adapter<VocabulariesAdapter.VocabulariesAdapterViewHolder> {
    private Context context;
    private List<Vocabulary> vocabularyList;

    public VocabulariesAdapter(Context context, List<Vocabulary> vocabularyList) {
        this.context = context;
        this.vocabularyList = vocabularyList;
    }

    public void setWords(List<Vocabulary> vocabularyList) {
        this.vocabularyList = vocabularyList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VocabulariesAdapter.VocabulariesAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.word_item, parent, false);
        return new VocabulariesAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VocabulariesAdapter.VocabulariesAdapterViewHolder holder, int position) {
        final Vocabulary vocabulary = vocabularyList.get(position);
        holder.english_word.setText(vocabulary.getEnglishWord());
        holder.english_example.setText(vocabulary.getEnglishExample());

        //on double clik show a bangla meaning of the word
        holder.english_word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, vocabulary.getBanglaMeaning(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        if(vocabularyList != null){
            return vocabularyList.size();
        }
        return 0;
    }

    public class VocabulariesAdapterViewHolder extends RecyclerView.ViewHolder {
        TextToSpeech tts;
        TextView english_word, english_example;
        ImageButton speakBtn;

        public VocabulariesAdapterViewHolder(View itemView) {
            super(itemView);
            english_word = (TextView) itemView.findViewById(R.id.english_word);
            english_example = (TextView) itemView.findViewById(R.id.english_example);
            speakBtn = itemView.findViewById(R.id.speakBtn);

            tts = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status != TextToSpeech.ERROR) {
                        tts.setLanguage(Locale.UK);
                    }
                }
            });
            speakBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String toSpeak = english_word.getText().toString();
                    tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                }
            });
        }
    }
}
