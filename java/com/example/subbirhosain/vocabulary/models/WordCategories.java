package com.example.subbirhosain.vocabulary.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WordCategories {

    @SerializedName("word_category_id")
    @Expose
    private int wordCategoryId;
    @SerializedName("word_category_name")
    @Expose
    private String wordCategoryName;

    public int getWordCategoryId() {
        return wordCategoryId;
    }

    public void setWordCategoryId(int wordCategoryId) {
        this.wordCategoryId = wordCategoryId;
    }

    public String getWordCategoryName() {
        return wordCategoryName;
    }

    public void setWordCategoryName(String wordCategoryName) {
        this.wordCategoryName = wordCategoryName;
    }

}
