
package com.example.subbirhosain.vocabulary.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Vocabulary {

    @SerializedName("word_id")
    @Expose
    private Integer wordId;
    @SerializedName("english_word")
    @Expose
    private String englishWord;
    @SerializedName("bangla_meaning")
    @Expose
    private String banglaMeaning;
    @SerializedName("english_example")
    @Expose
    private String englishExample;
    @SerializedName("word_category_id")
    @Expose
    private Integer wordCategoryId;

    public Integer getWordId() {
        return wordId;
    }

    public void setWordId(Integer wordId) {
        this.wordId = wordId;
    }

    public String getEnglishWord() {
        return englishWord;
    }

    public void setEnglishWord(String englishWord) {
        this.englishWord = englishWord;
    }

    public String getBanglaMeaning() {
        return banglaMeaning;
    }

    public void setBanglaMeaning(String banglaMeaning) {
        this.banglaMeaning = banglaMeaning;
    }

    public String getEnglishExample() {
        return englishExample;
    }

    public void setEnglishExample(String englishExample) {
        this.englishExample = englishExample;
    }

    public Integer getWordCategoryId() {
        return wordCategoryId;
    }

    public void setWordCategoryId(Integer wordCategoryId) {
        this.wordCategoryId = wordCategoryId;
    }

}
