
package com.example.subbirhosain.vocabulary.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Vocabularies {

    @SerializedName("vocabularies")
    @Expose
    private List<Vocabulary> vocabularies = null;
    @SerializedName("status")
    @Expose
    private Boolean status;

    public List<Vocabulary> getVocabularies() {
        return vocabularies;
    }

    public void setVocabularies(List<Vocabulary> vocabularies) {
        this.vocabularies = vocabularies;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

}
